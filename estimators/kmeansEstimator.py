from estimators.abstractEstimators import AbstractEstimators
from pipeline.table import Table
from pipeline.outils import Outils
import random as rd
import numpy as np
from transformers.normalisationTransformer import NormalisationTransformer
from copy import deepcopy
#import matplotlib.pyplot as plt

class kmeansEstimator(AbstractEstimators) : 
    """
    Estimateur qui va donner une classification de type K-means sur une table composee de variables numeriques. On peut choisir le nombre de classes et le nombre d'iteration de l'algorithme de Lloyd.
    Le choix des classes est effectue selon le critere de l'inertie intra-classe

    Args:
        nombre_classes (int): nombre de classes que l'on souhaite
        nstart (int, optional): nombre d'itération de l'algorithme de lloyd. Defaults to 30.
    """
    def __init__ (self, nombre_classes, nstart = 30) :
        """
        construit un estimateur de type K-means

        Args:
            nombre_classes (int): nombre de classes que l'on souhaite
            nstart (int, optional): nombre d'itération de l'algorithme de lloyd. Defaults to 30.
        """
        self.nombre_classes = nombre_classes
        self.nstart = nstart
    
    def fit(self, table) :
        """
        Estime les classes avec la méthode des K-means

        Args:
            nombre_classes (int): nombre de classes que l'on souhaite
            nstart (int, optional): nombre d'itération de l'algorithme de lloyd. Defaults to 30.

        Return :
        Table : la table normalisée avec la colonne des classes 
        """

        

        # normalisation de la table 
        normalisation = NormalisationTransformer()
        table_norm = normalisation.transform(table)

        # constante utilisable ensuite 
        nstart = self.nstart
        k = self.nombre_classes
        n = len(table_norm.rows[0])
        c = len(table_norm.rows)
        #liste des clusters 
        liste_clusters =[]
        liste_critere = []
        for iteration in range(nstart) :

            #création des listes dans lesquelles vont se trouver l'ancien et le nouveau centre 
            centres = [table_norm.rows[rd.randint(0,c-1)] for _ in range(k)]
            ancien_centres = [[0 for _ in range(len(centres[0]))] for _ in range(len(centres))]
            nouveaux_centres = deepcopy(centres)
            # liste classe et distance
            clusters = [0 for _ in range(c)]
            distances = [[0 for _ in range(c)] for _ in range(k)]

            #l'écart sert ici à voir si on a atteint un état stable 
            ecart = Outils.distance_vect(ancien_centres, nouveaux_centres)
            #boucle de création des classes 
            while Outils.egal_vect(ecart,[0 for i in range(len(ecart))]) == False :
                for i in range(k) :
                    # parcours de chacune des classes
                    for j in range(c) :
                        #parcours de chacun des points
                        distance = 0
                        for w in range (n) :
                            # parcours des coordonnées du point w 
                            
                            distance += (table_norm.rows[j][w] - nouveaux_centres[i][w])**2
                        distances[i][j] = distance**0.5
                clusters = np.argmin(distances, axis = 0)
                ancien_centres = deepcopy(nouveaux_centres)
                # on veut calculer les nouveaux centres
                # on parcourt les classes
                for i in range(k) :
                    #on veut les lignes de la classe i
                    classe_i = [table_norm.rows[j] for j in range(len(clusters)) if clusters[j] == i]
                    #print(classe_i)
                    table_classe_i = Table(["col" + str(f) for f in range(n)], classe_i)
                    tr_classe_i = table_classe_i.get_columns()
                    #centre de la classe i
                    centre_i = []
                    #on parcourt les colonnes 
                    for col in tr_classe_i :
                        if len(col) == 0: 
                            centre_i = ancien_centres[i]
                        else :
                            centre_i.append(Outils.mean(col))

                    nouveaux_centres[i] = centre_i       
                ecart = Outils.distance_vect(ancien_centres, nouveaux_centres)
            
            #initialise le critere inertie intra a 0
            critere = 0
            #on parcourt les classes
            for i in range(k):
                #on parcourt les lignes
                for j in range(c):
                    #si la ligne j appartient a la classe i
                    if clusters[j]== i :
                        #on parcourt les colonnes
                        for m in range(n):
                            #on prend la distance
                            critere += (table_norm.rows[j][m]-nouveaux_centres[i][m])**2
            liste_critere.append(critere)
            liste_clusters.append(clusters)  
        
        #on prend la valeur qui minimise le critere
        critere_min= min(liste_critere)
        #on prend son indice
        indice_min = liste_critere.index(critere_min)
        #on chosit le cluster correspondant
        cluster_best = liste_clusters[indice_min]
        new_table = table
        #on ajoute la colonne avec les classe a la table de depart
        new_table.add_column("classe",cluster_best)

        """ colors=['orange', 'blue', 'green']
        for i in range(c):
            plt.scatter(new_table.rows[i, 0], new_table.rows[i,1], s=7, color = colors[int(clusters[i])])
        plt.scatter(centres[:,0], centres[:,1], marker='*', c='g', s=150)
         """
        return(new_table)
