import unittest
from estimators.abstractEstimators import AbstractEstimators
from pipeline.table import Table
from pipeline.outils import Outils

class SumEstimator(AbstractEstimators):
    """
    estimateur de la somme pour des colonnes de la table choisies

    Args:
            column_names (list(str)): liste des colonnes dont on veut la somme. Default to None
                                      si None cela renvoie les moyennes de toutes les colonnes
    """

    def __init__(self,column_names=None):
        """
        construit un estimateur de la somme des colonnes choisies.
        Les colonnes doivent contenir uniquement des valeurs numeriques

        Args:
            column_names (list(str)): liste des colonnes dont on veut la somme. Default to None
                                      si None cela renvoie les moyennes de toutes les colonnes
        """
        self.__column_names = column_names

    def fit(self,table):
        """
        Estime la somme des colonnes chosies dans la table

        Args:
            table (Table): la table sur laquelle on veut la somme
        Return :
            Table : la table des sommes pour les différentes colonnes
        """
        #on recupere les colonnes de la table
        colonnes = table.get_columns()
        sommes = []
        if self.__column_names == None :
            for col in colonnes:
                sommes.append(Outils.sum(col))
            return Table(table.column_names,[sommes])
        else :
        #on parcourt les noms de colonnes choisis
            for name in self.__column_names :
                #on recupere la position du nom dans les noms de colonnes de la table
                pos = table.column_names.index(name)
                sommes.append(Outils.sum(colonnes[pos]))
        #on retourne la table avec les noms de colonnes choisis et en ligne les sommes
            return Table(self.__column_names,[sommes])


