from pipeline.abstractOperation import AbstractOperation
from abc import abstractmethod


class AbstractEstimators(AbstractOperation):
    """
    Classe abstraite dont vont hériter tous les estimateurs. 

    """
    def __init__(self) :
        pass

    @abstractmethod
    def fit(self, table):
        """methode abstraite qui sera implementee pour les classes d'estimateurs

        """
        pass

    def process(self, table):
        """execute la methode fit sur le tableau en parametre 

        Args:
            table (Table): le tableau sur lequel on veut un estimateur
        """
        return self.fit(table)