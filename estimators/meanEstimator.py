from estimators.abstractEstimators import AbstractEstimators
from pipeline.table import Table
from pipeline.outils import Outils

class MeanEstimator(AbstractEstimators):
    """
    estimateur de la moyenne pour des colonnes de la table choisies
    
    Args:
        column_names (list(str)): liste des colonnes dont on veut la moyenne. Default to None
                                   si None cela renvoie les moyennes de toutes les colonnes
    """

    def __init__(self,column_names=None):
        """
        construit un estimateur de la moyenne sur des colonnes choisies.
        Les colonnes doivent contenir uniquement des valeurs numeriques

        Args:
            column_names (list(str)): liste des colonnes dont on veut la moyenne. Default to None
                                      si None cela renvoie les moyennes de toutes les colonnes
        """
        self.__column_names = column_names

    def fit(self,table):
        """
        Estime la moyenne la moyenne des colonnes chosies dans la table

        Args:
            table (Table): la table sur laquelle on veut la moyenne
        Return :
            Table : la table avec les moyennes pour les differentes colonnes
        """
        #on recupere les colonnes de la table
        colonnes = table.get_columns()
        moyennes = []
        if self.__column_names == None :
            for col in colonnes:
                moyennes.append(Outils.mean(col))
            return Table(table.column_names,[moyennes])
        #on parcourt les noms de colonnes choisis
        else :
            for name in self.__column_names :
                #on recupere la position du nom dans les noms de colonnes de la table
                pos = table.column_names.index(name)
                moyennes.append(Outils.mean(colonnes[pos]))
        #on retourne la table avec les noms de colonnes choisis et en ligne les moyennes
            return Table(self.__column_names,[moyennes])

