from estimators.abstractEstimators import AbstractEstimators
from pipeline.table import Table
from pipeline.outils import Outils

class SdEstimator(AbstractEstimators):
    """
    estimateur de la l'ecart type pour des colonnes de la table choisies

    Args:
    columns_names (list(str)): liste des colonnes dont on veut l' ecart type. Default to None
                                      si None cela renvoie les moyennes de toutes les colonnes
    """

    def __init__(self,column_names=None):
        """
        construit un estimateur de l'ecart type sur des colonnes choisies.
        Les colonnes doivent contenir uniquement des valeurs numeriques

        Args:
            column_names (list(str)): liste des colonnes dont on veut l' ecart type
        """
        self.__column_names = column_names

    def fit(self,table):
        """
        Estime l'ecart type des colonnes chosies dans la table

        Args:
            table (Table): la table sur laquelle on veut l'ecart type
        Return :
            Table : la table avec les ecarts types
        """
        #on recupere les colonnes de la table
        colonnes = table.get_columns()
        sd_cols = []
        if self.__column_names == None :
            for col in colonnes:
                sd_cols.append(Outils.sd(col))
            return Table(table.column_names,[sd_cols])
        else :
            #on parcourt les noms de colonnes choisis
            for name in self.__column_names :
                #on recupere la position du nom dans les noms de colonnes de la table
                pos = table.column_names.index(name)
                sd_cols.append(Outils.sd(colonnes[pos]))
        #on retourne la table avec les noms de colonnes choisis et en ligne les moyennes
            return Table(self.__column_names,[sd_cols])