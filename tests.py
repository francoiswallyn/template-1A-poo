import unittest
from estimators.sumEstimator import SumEstimator
from transformers.selectionTransformer import SelectionTransformer
from transformers.jointureTransformer import JointureTransformer
from pipeline.table import Table

class SumTest(unittest.TestCase):
    def test_sum(self):
        table = Table(["col1","col2","col3"],[["a",1,2],["b",3,5],["c",4,8]])
        table_sum = SumEstimator(["col2","col3"]).fit(table)
        self.assertEqual(["col2","col3"],table_sum.column_names)
        self.assertEqual([8.0,15.0],table_sum.rows)



class SelectionTest(unittest.TestCase):
    def test_selection(self) :
        table = Table(["col1","col2","col3"],[["a",1,2],["b",3,5],["c",4,8],["a",8,4]])
        selec = SelectionTransformer(liste_colonnes = ["col1","col3"],var_id_interval = {"col2" : ["sup",2]},var_id_exact = {"col1": ["a"]})
        table_selec = selec.transform(table)
        self.assertEqual(["col1","col3"],table_selec.column_names)
        self.assertEqual([["a",4]],table_selec.rows)

class JointureTest(unittest.TestCase):
    def test_left_join(self):
        table_depart = Table(["dep","indic"],[["59",4],["35",5],["59",8],["22",7],["35",9]])
        table_to_join = Table(["dep","reg"],[["59","HDF"],["35","BRET"],["22","BRET"]])
        joint = JointureTransformer(table = table_to_join,cle_primaire ="dep",cle_primaire2="dep",typejointure ="left")
        table_join = joint.transform(table_depart)
        self.assertEqual(["dep","indic","reg"],table_join.column_names)
        self.assertEqual([['59', 4, 'HDF'], ['35', 5, 'BRET'], ['59', 8, 'HDF'], ['22', 7, 'BRET'], ['35', 9, 'BRET']],table_join.rows)

    def test_inner_join(self):
        table_depart = Table(["dep","indic1"],[["59",4],["35",5],["13",8],["22",7],["62",9]])
        table_to_join = Table(["dep","indic2"],[["59",1],["35",3],["22",5],["13",2],["62",9]])
        joint = JointureTransformer(table = table_to_join,cle_primaire ="dep",cle_primaire2="dep",typejointure ="inner")
        table_join = joint.transform(table_depart)
        self.assertEqual(["dep","indic2","indic1"],table_join.column_names)
        lignes_attendues  = [['59', 1, 4], ['35', 3, 5], ['13', 2, 8], ['22', 5, 7], ['62', 9, 9]]
        for ligne in lignes_attendues:
            self.assertIn(ligne,table_join.rows)


if __name__ == '__main__':
    unittest.main()