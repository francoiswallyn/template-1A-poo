# -*- coding: utf-8 -*-
import csv
import numpy as np
import random as rd
import datetime
from datetime import date, timedelta
from copy import deepcopy
from transformers.chargementTransformer import ChargementTransformer
from transformers.centrageTransformer import CentrageTransformer
from transformers.reductionTransformer import ReductionTransformer
from transformers.normalisationTransformer import NormalisationTransformer
from transformers.selectionTransformer import SelectionTransformer
from transformers.jointureTransformer import JointureTransformer
from transformers.moyenneGlissanteTansformer import MoyenneGlissanteTransformer
from transformers.applicationSousTableTransformer import ApplicationSousTableTransformer
from transformers.exportCsvTransformer import ExportCsvTransformer
from transformers.exportMapTransformer import ExportMapTransformer
from estimators.meanEstimator import MeanEstimator
from estimators.sumEstimator import SumEstimator
from estimators.sdEstimator import SdEstimator
from pipeline.pipeline import Pipeline
from pipeline.table import Table
from pipeline.outils import Outils
from estimators.kmeansEstimator import kmeansEstimator
from transformers.agregationspatialTransformer import AgregationspatialTransformer

if __name__ == "__main__":

    
    """ print("Exemple de Kmeans avec moyenne par classe")
    center_1 = [100,100]
    center_2 = [-100,-100]
    center_3 = [0,0]
    data_2 =[[rd.randint(0, 50) + center_1[0] , rd.randint(0,50) + center_1[1]] for i in range(10)] 
    data_1 =[[rd.randint(0, 50) + center_2[0] , rd.randint(0,50) + center_2[1]] for i in range(10)]
    data_3 =[[rd.randint(0, 50) + center_3[0] , rd.randint(0,50) + center_3[1]] for i in range(10)] 
    liste = data_1 + data_2 + data_3
    table_dep_kmean = Table(["x", "y"], liste) 

    pipe_kmeans = Pipeline([kmeansEstimator(3),ApplicationSousTableTransformer("classe","moyenne")])
    table_moy_par_classe = pipe_kmeans.run(table_dep_kmean)
    print(table_moy_par_classe.column_names) 
    print(table_moy_par_classe.rows)
    


    """
    """
    Les questions à coder :
    
    1) Quel est le nombre total d’hospitalisations dues au Covid-19 ?
    """
    """
    print("Quel est le nombre total d’hospitalisations dues au Covid-19 ?")
    pipe1 = Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv",type_fichier ="csv",types_var=["str","date","int","int","int","int"]),SumEstimator(["incid_hosp"])])
    table1 = pipe1.run()
    print(table1.column_names)
    print(table1.rows)
    """
    """
    2) Combien de nouvelles hospitalisations ont eu lieu ces 7 derniers jours dans chaque departement?
    """
    """
    print("Combien de nouvelles hospitalisations ont eu lieu ces 7 derniers jours dans chaque departement?")
    pipe2 = Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv",type_fichier ="csv",types_var=["str","date","int","int","int","int"]),SelectionTransformer(var_id_interval = {"jour":[date(2021,2,25),date(2021,3,3)]}),ApplicationSousTableTransformer(var_id = "dep",func = "somme",cols = ["incid_hosp"]),ExportMapTransformer(nom = "hosp_semaine_dep",echelle = "dep",nom_colonne_echelle = "dep",nom_colonne_plot = "somme incid_hosp",is_metropole = True),ExportCsvTransformer(".","hosp_dep_last_week")])
    table2 = pipe2.run()
    print(table2.column_names) 
    print(table2.rows)
    """
    """
    3) Comment évolue la moyenne des nouvelles hospitalisations journalières de cette semaine par rapport à celle de la semaine dernière ?
    """
    """
    print("Comment évolue la moyenne des nouvelles hospitalisations journalières de cette semaine par rapport à celle de la semaine dernière ?")
    #Derniere semaine
    pipe3_1 = Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv",type_fichier ="csv",types_var=["str","date","int","int","int","int"]),SelectionTransformer(liste_colonnes= ["incid_hosp"],var_id_interval = {"jour":[date(2021,2,25),date(2021,3,3)]}),MeanEstimator()])
    data3_1 = pipe3_1.run()
    print("Moyenne des nouvelles hospitalisation de la derniere semaine")
    print(data3_1.column_names)
    print(data3_1.rows)

    #Avant derniere semaine 
    pipe3_2 = Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv",type_fichier ="csv",types_var=["str","date","int","int","int","int"]),SelectionTransformer(liste_colonnes= ["incid_hosp"],var_id_interval = {"jour":[date(2021,2,18),date(2021,2,24)]}),MeanEstimator()])
    data3_2 = pipe3_2.run()
    print("Moyenne des nouvelles hospitalisation de l'avant derniere semaine")
    print(data3_2.column_names)
    print(data3_2.rows) 
    """
    """
    4) Quel est le résultat de k-means avec k = 3 sur les données des départements du mois de Janvier 2021, lissées avec une moyenne glissante de 7 jours ?
    """
    """
    print("Quel est le résultat de k-means avec k = 3 sur les données des départements du mois de Janvier 2021, lissées avec une moyenne glissante de 7 jours ?")
    pipe4_1 = Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv",type_fichier ="csv",types_var=["str","date","int","int","int","int"]),SelectionTransformer(var_id_interval = {"jour":[date(2021,1,1),date(2021,1,31)]})])
    table_janvier = pipe4_1.run()
    table_dep = Table([],[])
    levels = table_janvier.levels("dep")
    for dep in levels :
        table_janvier_copy = deepcopy(table_janvier)
        pipe = Pipeline([SelectionTransformer(liste_colonnes =["incid_hosp","incid_rea","incid_dc","incid_rad"],var_id_exact = {"dep": [dep]}),MoyenneGlissanteTransformer(7)])
        table_new = pipe.run(table_janvier_copy)
        #print(table_new.rows)
        names =[]
        ligne =[]
        for i in range(len(table_new.column_names)) :
            n =0
            for row in table_new.rows :
                ligne.append(row[i])
                n+=1
                names.append(table_new.column_names[i] + str(n))
        
        table_dep.column_names = names
        table_dep.add_row(ligne)
    pipe4_2 = Pipeline([kmeansEstimator(3)])
    table_kmeans = pipe4_2.run(table_dep)
    table_kmeans.add_column("dep",levels)
    pipe4_3 = Pipeline([SelectionTransformer(["dep","classe"]),ExportMapTransformer(nom = "kmeans_dep",echelle = "dep",nom_colonne_echelle = "dep",nom_colonne_plot = "classe",is_metropole = True)])
    table_dep_classes = pipe4_3.run(table_kmeans)
    print(table_dep.rows) 
   
    """
    """
    5) Combien de nouvelles admissions en réanimation ont eu lieu pendant la semaine suivant les vacances de la Toussaint de 2020 ?
    """
    """
    print("Combien de nouvelles admissions en réanimation ont eu lieu pendant la semaine suivant les vacances de la Toussaint de 2020 ?")
     #etape 1 = Trouver date des vacances scolaires Toussain
    pipe5_1=Pipeline([ChargementTransformer("./VacancesScolaires.json", "json",nom_feuille="Calendrier"),SelectionTransformer(["Fin"],var_id_exact={"Description":["Vacances de la Toussaint"],'annee_scolaire': ["2020-2021"],'Zone':["Zone A"]}),SelectionTransformer(liste_lignes=[0])])
    data=pipe5_1.run()
    date_debut=date.fromisoformat(data.rows[0][0])
    print("Date fin de vacances de la Toussaint zones métropole : ",date_debut)

    #etape2 Répond à la question
    data2=ChargementTransformer("./VacancesScolaires.json", "json",nom_feuille="Academie").transform()

    
    
    print("Nombre d'admissions de réanimation : ")
    pipe5_3=Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv", type_fichier="csv",types_var=["str","date","int","int","int","int"]),JointureTransformer(data2,"dep","Code_Dpt",typejointure="left"),SelectionTransformer(liste_colonnes=["incid_rea"],var_id_interval={'jour':[date_debut,date_debut+timedelta(days=6)]},var_id_exact = {'Zone':["Zone A","Zone B","Zone C","Corse"]}),SumEstimator()])
    data3=pipe5_3.run()
    print(data3.column_names)
    print(data3.rows) 
    
    pipe5_4=Pipeline([ChargementTransformer("./donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv", type_fichier="csv",types_var=["str","date","int","int","int","int"]),
        JointureTransformer(Pipeline([ChargementTransformer("./VacancesScolaires.json", "json",nom_feuille="Academie")]).run(),"dep","Code_Dpt",typejointure="left"),
        SelectionTransformer(liste_colonnes=["incid_rea"],var_id_interval={'jour':[date.fromisoformat(Pipeline([ChargementTransformer("./VacancesScolaires.json", "json",nom_feuille="Calendrier"),
        SelectionTransformer(["Fin"],var_id_exact={"Description":["Vacances de la Toussaint"],'annee_scolaire': ["2020-2021"],'Zone':["Zone A"]}),
        SelectionTransformer(liste_lignes=[0])]).run().rows[0][0]),date.fromisoformat(Pipeline([ChargementTransformer("./VacancesScolaires.json", "json",nom_feuille="Calendrier"),
        SelectionTransformer(["Fin"],var_id_exact={"Description":["Vacances de la Toussaint"],'annee_scolaire': ["2020-2021"],'Zone':["Zone A"]}),
        SelectionTransformer(liste_lignes=[0])]).run().rows[0][0])+timedelta(days=6)]},var_id_exact = {'Zone':["Zone A","Zone B","Zone C","Corse"]}),
        SumEstimator()])
    data3_1 = pipe5_4.run()
    print(data3_1.column_names)
    print(data3_1.rows)  """
    
    chemin_json = "./VacancesScolaires.json"
    pipe_reg = Pipeline([ChargementTransformer(chemin_fichier = chemin_json,type_fichier= "json",types_var=None,nom_feuille = "Academie")])
    table_academie = pipe_reg.run()

    chemin_csv_covid = "./donnees-hospitalieres-covid19-2021-03-03-17h03.csv"
    pipe_end = Pipeline(steps = [ChargementTransformer(chemin_fichier=chemin_csv_covid,type_fichier="csv",types_var=["str","str","date","int","int","int","int"]),
                                SelectionTransformer(var_id_interval={"jour":[date(2021,1,1),date(2021,1,31)]},var_id_exact={"sexe":["2"]}),
                                JointureTransformer(table_academie,"dep","Code_Dpt",typejointure="left"),
                                ApplicationSousTableTransformer("Region","moyenne",["rea"]),
                                ExportCsvTransformer(".","moyenne_rea_femme_reg_janv"),
                                ExportMapTransformer(nom="moyenne_rea_femme_reg_janv",echelle="reg",nom_colonne_echelle="Region",is_metropole=True)])
    

    table_end = pipe_end.run()



    

   