from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils
from carte.cartoplot import CartoPlot

class ExportMapTransformer (AbstractTransformers) :
    """
    Represente un indicateur sur une carte par departement ou par region. Il faut que la table contienne 
    les code de departements ou regions ainsi que l'indicateur associé.

    Args:
        nom (str): nom de la carte
        echelle (str, optional): decoupage geographique soit "dep" soit "reg". Defaults to "dep".
        nom_colonne_echelle (str, optional): nom de la colonne qui donne le decoupage geographique dans la table. Defaults to "dep".
        nom_colonne_plot (str, optional): nom de la colonne de l'estimateur que l'on veut representer. Defaults to None.
                                          si None cela prend l'autre colonne que celle de nom_colonne_echelle
        is_metropole (bool, optional): indique si on veut representer uniquement la metropole . Defaults to False.
    
    Examples:
    >>> maTable = Table(["dep","nom_indicateur"],[["01",2],["02",5],...,["976",8]])
    >>> expmap = ExportMapTransformer(nom = "map_name",echelle = "dep",nom_colonne_plot = "nom_indicateur",is_metropole = True)
    >>> expmap.transform(maTable)
    """

    def __init__ (self, nom,echelle = "dep",nom_colonne_echelle = "dep",nom_colonne_plot=None, is_metropole = False) :
        """
        Construit un transformateur qui exporte les donnees sur une carte a l'echelle des departements ou des regions.

        Args:
            nom (str): nom de la carte
            echelle (str, optional): decoupage geographique soit "dep" soit "reg". Defaults to "dep".
            nom_colonne_echelle (str, optional): nom de la colonne qui donne le decoupage geographique dans la table. Defaults to "dep".
            nom_colonne_plot (str, optional): nom de la colonne de l'estimateur que l'on veut representer. Defaults to None.
                                              si None cela prend l'autre colonne que celle de nom_colonne_echelle
            is_metropole (bool, optional): indique si on veut representer uniquement la metropole . Defaults to False.
        """
        self.__nom = nom
        self.__echelle = echelle
        self.__nom_colonne_echelle = nom_colonne_echelle
        self.__nom_colonne_plot =nom_colonne_plot
        self.__is_metropole = is_metropole

    
    def transform(self,table) : 
        """
        Represente un indicateur sur une carte.

        Args:
            table (Table): la table constituee du nom du decoupage geographiqe et de l'indicateur qu'on veut representer
        
        Returns :
            Table : la table non modifiee
        """
        carte_plot = CartoPlot() 
        dic = {}
        pos_echelle = table.column_names.index(self.__nom_colonne_echelle)
        if self.__nom_colonne_plot == None :
            for i in range(len(table.column_names)):
                if i != pos_echelle :
                    pos_var = i
        else :
            pos_var = table.column_names.index(self.__nom_colonne_plot)
        for row in table.rows :
            if row[pos_echelle] == "69" and self.__echelle == "dep":
                dic["69D"]= row[pos_var]
                dic["69M"] = row[pos_var]
            else :
                dic[row[pos_echelle]]=row[pos_var]
        print(dic)
        if self.__echelle == "dep":
            if self.__is_metropole :
                fig = carte_plot.plot_dep_map(data = dic,x_lim=(-6, 10), y_lim=(41, 52))
                fig.savefig(self.__nom + ".jpg")
                fig.show()
            else :
                fig = carte_plot.plot_dep_map(data = dic,)
                fig.savefig(self.__nom + ".jpg")
                fig.show()

        elif self.__echelle == "reg": 
            if self.__is_metropole : 
                fig = carte_plot.plot_reg_map(data = dic,show_name = False,x_lim=(-6, 10), y_lim=(41, 52))
                fig.savefig(self.__nom + ".jpg")
                fig.show()
            else :
                fig = carte_plot.plot_reg_map(data = dic,show_name = False)
                fig.savefig(self.__nom + ".jpg")
                fig.show()

        return(table)