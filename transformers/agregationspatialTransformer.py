from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
import copy as cp
import json

class AgregationspatialTransformer(AbstractTransformers):
    """
    Transformeur qui va agreger les donnees departementale en donnees regionales. Pour chaque colonne on a 
    la somme des differents departement de la region.

    """
    def __init__(self,positionagregat,position_commun,position_var,chemin_fichier_json):
        """
        Construit un transformeur qui va agreger les donnees departementale en donnees regionales 
        a partir d'un fichier json (le fichier json des vacances scolaires)

        Args:
            positionagregat (int): numero de colonne du departement 
            position_commun (list(int)): numeros de colonnes des variable non agregee
            position_var (list(int)): numeros des colonnes qui sont agreger (on fait la somme)
            chemin_fichier_json (str): chemin du fichier json des vacances scolaires
        """
        
        
        #position variable voulant etre agrégée : numeric
        self.positionagregat=positionagregat
        #position  commun : liste de numeric
        self.position_commun=position_commun
        #position variable  : liste de numeric
        self.position_var=position_var
        #chemin fichier json Vacances Scolaires
        self.chemin_fichier_json=chemin_fichier_json

    def __dictagrega(self):
        """
        Construit le dictionnaires des correspondances entre les departement et les regions
        """
        with open(self.chemin_fichier_json,encoding = 'UTF-8') as json_file :
            Tableau = json.load(json_file)
        res={}
        for i in Tableau["Academie"]:
            res[i["Code_Dpt"]]=i["Region"]
        return(res)

    def transform(self,table):
        """
        Transforme la table de depart en agregeant les donnees departementales en donnees regionales

        Args:
            table (Table): la table que l'on souhaite agreger
        """
        dictagregation=self.__dictagrega()
        trans=table.get_columns()
        #table de sortie
        Newtable=Table([],[])

        Newtable.column_names=["Region"]+[table.column_names[i] for i in self.position_commun]+[table.column_names[i] for i in self.position_var]
        compteur=0
        #liste des n+1 uplet present dans new table
        listePresent=[]
        for agr in trans[self.positionagregat]:
            if len(agr)==1:
                agr="0"+agr
            #mise dans newagre le nom de la region de agr
            newagre=dictagregation[agr]
            if ([newagre]+[table.rows[compteur][i] for i in self.position_commun]) not in listePresent:
                Newtable.add_row([newagre]+[table.rows[compteur][i] for i in self.position_commun]+[table.rows[compteur][i] for i in self.position_var])
                listePresent.append([newagre]+[table.rows[compteur][i] for i in self.position_commun])
            else:
                #trouver la position de cette ligne
                positionind=listePresent.index([newagre]+[table.rows[compteur][i] for i in self.position_commun])
                for j in self.position_var:
                    namecollumn=table.column_names[j]
                    pos=table.column_names.index(namecollumn)
                    Newtable.rows[positionind][j]+=table.rows[compteur][pos]
                

            
            compteur+=1
        return(Newtable)