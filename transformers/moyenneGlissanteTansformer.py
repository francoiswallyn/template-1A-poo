from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils

class MoyenneGlissanteTransformer(AbstractTransformers):
    """
    transformeur qui renvoie la moyenne glissante sur une periode donnee.
    Args:
        period (int): periode sur laquelle on veut faire la moyenne glissante
                        doit etre impaire
    """

    def __init__(self,period):
        """
        tranformeur qui renvoie le tableau avec la moyenne glissante sur une periode
        pour chaque valeur. La periode doit etre un entier impaire

        Args:
            period (int): periode sur laquelle on veut faire la moyenne glissante
                          doit etre impaire
        """
        self.__period = period

    def moyenne_glissante(self,liste):
        """
        renvoie la liste des moyennes glissantes sur une periode choisie pour une liste de reels.
        La periode doit etre impaire. 

        Args:
            liste (list(float or int)): une liste de reels

            period (int impaire) : la periode (impaire)
        
        Returns :
            list(float) : la liste des moyennes glissantes
        """
        n = len(liste)
        moyenne_glissante = []
        first_pos = self.__period//2
        last_pos = n - 1 - self.__period//2
        for i in range(n):
            fenetre = []
            if first_pos <= i <= last_pos :
                for j in range(i-self.__period//2,i+ self.__period//2 +1) :
                    fenetre.append(liste[j])
                moyenne_glissante.append(Outils.mean(fenetre))
        return moyenne_glissante

    def transform(self,table):
        """
        renvoie la table avec pour chaque valeur la moyenne glissante sur la periode choisie

        Args:
            table (Table): la table sur laquelle on veut faire la moyenne glissante
        
        Returns : 
            Table : la table lissee 
        """
        #on recupere les colonnes
        colonnes = table.get_columns()
        moy_glissante_cols =[]
        #on parcourt les colonnes
        for col in colonnes :
            #on fait les moyennes glissantes sur chaque colonne
            moy_glissante_cols.append(self.moyenne_glissante(col))
        #on reconstitue les moyennes glissantes dans un tableau
        rows = []
        #on parcourt les indices de du premier element de moy_glissante_cols (qui donne le nombre de ligne de la nouvelle table)
        for i in range(len(moy_glissante_cols[0])) :
            row =[]
            #on parcourt les listes de moyennes glissantes pour chaque colonne
            for col in moy_glissante_cols :
                row.append(col[i])
            rows.append(row)
        table_transformed = Table(table.column_names,rows)

        return table_transformed