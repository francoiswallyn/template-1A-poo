from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
import copy as cp


class SelectionTransformer(AbstractTransformers):
    """Selectionne une sous table de la table de depart.
    La selection peut etre faite selon une/des colonnes, des lignes, des variables
    identifiantes (intervalle de valeurs, ou valeurs exactes). 
    1) Il va d'abord selectionner les lignes
    2) Ensuite choisir le tableau selon la/les variables identifiante.
    3) Enfin selectionner les colonnes
    Il n'est cependant pas pertinent de selectionner en meme temps des lignes
    et une variable identifiante.

    Args :
        liste_colonnes (list(str), optional): le nom des colonnes que l'on souhaite garder. Defaults to None.
        liste_lignes (list(int), optional): la position des lignes que l'on souhaite garder. Defaults to None.
        var_id_interval (dict, optional): dictionnaire avec pour cles les noms des colonnes et pour valeurs l'intervalle des valeurs qu'on veut garder. Defaults to None.
                                    si on veut que se soit egal a une valeur on met juste la valeur
                                    si on veut les valeurs inferieures a une valeur on met ["inf",val]
                                    si on veut les valeurs superieures a une valeur on met ["sup",val]
                                    si on les valeurs entre 2 valeurs on met [val_min,val_max]
        var_id_exact (dict,optional) : dictionnaire avec pour cles les noms des colonnes et pour valeurs la liste des valeurs qu'on veut garder. Defaults to None. 

    Examples:
    >>> table = Table(["col1","col2","col3"], [[2,"T",2],[10,"F",3],[9,6,9],[5,"T",6],[1,"H",3],[4,"T",10]])
    >>> select = SelectionTransformer(liste_colonnes = ["col1,"col2"],var_id_interval = {"col3": ["sup",5]},var_id_exact = {"col2":["T"]})
    >>> new_table = select.transform(table)
    >>> print(new_table.columns_name)
    ["col1","col3"]
    >>> print(new_table.rows)
    [[1,3],[4,10]]
    """

    def __init__(self, liste_colonnes=None, liste_lignes=None, var_id_interval=None, var_id_exact=None):
        """construit un transformeur qui selectionne un sous tableau selon des colonnes
        (nom des colonnes), liste_ligne (positions), variables identifiantes
        (nom colonne et valeur).
        Il va d'abord selectionner les lignes
        Ensuite choisir le tableau selon la/les variables identifiante.
        Enfin selectionner les colonnes
        Il n'est cependant pas pertinent de selectionner en meme temps des lignes
        et une variable identifiante.

        Args:
            liste_colonnes (list(str), optional): le nom des colonnes que l'on souhaite garder. Defaults to None.
            liste_lignes (list(int), optional): la position des lignes que l'on souhaite garder. Defaults to None.
            var_id_interval (dict, optional): dictionnaire avec pour cles les noms des colonnes et pour valeurs l'intervalle des valeurs qu'on veut garder. Defaults to None.
                                     si on veut que se soit egal a une valeur on met juste la valeur
                                     si on veut les valeurs inferieures a une valeur on met ["inf",val]
                                     si on veut les valeurs superieures a une valeur on met ["sup",val]
                                     si on les valeurs entre 2 valeurs on met [val_min,val_max]
            var_id_exact (dict,optional) : dictionnaire avec pour cles les noms des colonnes et pour valeurs la liste des valeurs qu'on veut garder. Defaults to None. 
        """
        self.__liste_colonnes = liste_colonnes
        self.__liste_lignes = liste_lignes
        self.__var_id_interval = var_id_interval
        self.__var_id_exact = var_id_exact

    def selection_lignes(self, table):
        """selectionne les lignes du tableau que l'on a choisies dans liste_lignes

        Args:
            table (Table): la table sur laquelle on veut faire la selection
        """
        n=0
        ex_pos = len(table.rows)
        for pos in range(len(table.rows)):
            if pos not in self.__liste_lignes : 
                if pos < ex_pos :
                    table.remove_row(pos)
                    n+=1
                else :
                    table.remove_row(pos-n)
                    n+=1

            ex_pos = pos
    def selection_colonnes(self, table):
        """selectionne les colonnes du tableau que l'on a choisies dans liste_colonnes (nom de colonnes).

         Args:
             table (Table): la table sur laquelle on veut faire la selection
         """
        colnames = cp.deepcopy(table.column_names)
        for name in colnames:
            if name not in self.__liste_colonnes:
                table.remove_column_name(name)

    def selection_var_id_interval(self, table):
        """selectionne les lignes dont la valeur de la colonne en cle de var_id correspond a la valeur chosie pour cette colonne.
        Toutes les colonnes sont gardees.

        Args:
            table (Table): la table sur laquelle on veut faire la selection
        """
        for name in self.__var_id_interval.keys():
            pos = table.column_names.index(name)
            val = self.__var_id_interval[name]
            if val[0]=="inf" :
                val_max = val[1]
                #on fait une copie des lignes 
                rows = cp.deepcopy(table.rows)
                #on parcourt les lignes
                for row in rows :
                    if row[pos]>val_max :
                        table.remove_row(table.rows.index(row))
        
            #sinon si on veut des valeurs superieures a un nombre 
            elif val[0]=="sup" :
                val_min = val[1]
                #on fait une copie des lignes 
                rows = cp.deepcopy(table.rows)
                #on parcourt les lignes
                for row in rows :
                    if row[pos]<val_min :
                        table.remove_row(table.rows.index(row))
            #sinon on veut des valeurs entre deux nombres
            else :
                val_min = val[0]
                val_max = val[1]
                #on fait une copie des lignes
                rows = cp.deepcopy(table.rows)
                for row in rows :
                    if row[pos]<val_min or row[pos]>val_max:
                        table.remove_row(table.rows.index(row))
    
    def selection_var_id_exact(self, table):

        """selectionne les lignes dont la valeur de la colonne en cle de var_id correspond a la valeur chosie pour cette colonne.
        Toutes les colonnes sont gardees.

        Args:
        table (Table): la table sur laquelle on veut faire la selection
        """

        for name in self.__var_id_exact.keys():
            pos = table.column_names.index(name)
            val = self.__var_id_exact[name]
            #print(val[0])
            #si la valeur est unique
                #on fait une copie des lignes 
            rows = cp.deepcopy(table.rows)
            #on parcourt les lignes de la table
            for row in rows :
                #si la valeur de la colonne choisie pour la ligne est differente de la valeur chosie
                if row[pos] not in val :
                    #on retire la ligne
                    table.remove_row(table.rows.index(row))

                
    def transform(self,table):
        """transformeur qui selectionne le sous tableau a partir des lignes, colonnes, et variables identifiantes choisies

        Args:
            table (Table): la table que l'on veut transformer
        Return :
            Table : la sous-table selectionnee
        """
        if self.__liste_lignes != None :
            self.selection_lignes(table)

        if self.__var_id_interval != None :
            self.selection_var_id_interval(table)
        if self.__var_id_exact!= None :
            self.selection_var_id_exact(table)
        if self.__liste_colonnes != None :
            self.selection_colonnes(table)
        return table