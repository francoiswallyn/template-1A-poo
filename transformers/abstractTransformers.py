from pipeline.abstractOperation import AbstractOperation
from abc import abstractmethod

class AbstractTransformers(AbstractOperation):
    """
    Classe abstraite dont les transformeurs vont heriter.

    """
    def __init__(self) :
        pass

    @abstractmethod
    def transform(self,table):
        """
        methode abstraite qui sera implementee pour les classes de transformeurs

        Args:
            table (Table): table que l'on veut transformer
        """
        pass
    
    def process(self,table) :
        """
        execute la methode transform sur le tableau en parametre 

        Args:
            table (Table): le tableau que l'on veut transformer
        """
        return self.transform(table)