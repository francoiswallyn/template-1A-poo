from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils

class CentrageTransformer(AbstractTransformers) :
    """
    transformeur qui va centrer un tableau ayant des valeurs numeriques
    """

    def __init__(self):
        """
        construit un transformeur qui va centrer un tableau 
        """
        pass 

    def centre(self,table):
        """
        renvoie la liste des centres de chaque colonne

        Args:
            table (Table): la table dont on veut les centres
        """
        colonnes = table.get_columns()
        centres = []
        for col in colonnes :
            centres.append(Outils.mean(col))
        return centres

    def transform(self,table) :
        """
        transformeur qui centre la table en entree.
        Il faut que la table soit composee de valeurs numeriques.

        Args:
            table (Table): la table que l'on veut centrer

        Return:
            Table : la table centree 
        """
        #On calcule les centres pour chaque colonne
        centre = self.centre(table)
        #On parcourt les lignes
        for i in range(len(table.rows)) :
            #on parcourt les colonnes
            for j in range(len(table.column_names)):
                #on centre l'element de la ligne i pour la colonne j 
                table.rows[i][j] = table.rows[i][j] - centre[j]
        return table 