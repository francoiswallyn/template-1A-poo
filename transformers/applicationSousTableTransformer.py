from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils
from transformers.selectionTransformer import SelectionTransformer

class ApplicationSousTableTransformer(AbstractTransformers) :
    """
    transformeur separe la table par rapport  a une variable identifiante, applique une fonction à chaque sous-table,
    et renvoie la table après application

    Args:
            var_id (str): nom de la colonne qui va serivir de variable identifiante
            func (str): peut-être "somme" ou "moyenne", appliquees a chaque sous table
            cols (list(str)) : liste des noms colonnes sur lesquelles on veut appliquer la fonction
    """


    def __init__(self, var_id, func,cols = None):
        """
        construit transformateur qui applique une fonction à chaque sous-table

        Args:
            var_id (str): nom de la colonne qui va serivir de variable identifiante
            func (str): peut-être "somme" ou "moyenne", appliquees a chaque sous table
            cols (list(str)) : liste des noms colonnes sur lesquelles on veut appliquer la fonction
        """
        self.__var_id = var_id
        self.__func = func
        self.__cols = cols

    
    def transform(self,table):
        """
        transforme la table en la separant par variable identifiante, applique une fonction (somme ou moyenne),
        renvoie la table ou chaque ligne correspond a une variable identifiante et les colonnes correspondent 
        à la fonction appliquee sur les colonnes chosie. 

        Args:
            table (Table): la table que l'on veut transformer

        Returns :
            Table : la table a laquelle on a appliquee une fonction sur chaque sous table de la table de depart
        """
        if self.__cols == None :
            table_new = table
            cols = [colname for colname in table.column_names if colname!= self.__var_id]
            pos_col = [table_new.column_names.index(col) for col in cols]
        else : 
            table_new = SelectionTransformer([self.__var_id] + self.__cols).transform(table)
            cols = self.__cols
            pos_col = [table_new.column_names.index(col) for col in self.__cols]
        pos_var_id = table_new.column_names.index(self.__var_id)
        rows =[]
        liste_differents = []
        colonnes = table_new.get_columns()
        nbcols = len(cols)
        if self.__func == "somme" :
            for var in colonnes[pos_var_id]:
                somme = [0 for _ in range(nbcols)]
                if var not in liste_differents :
                    liste_differents.append(var)
                    for i in range(len(colonnes[pos_var_id])):
                        for j in range(nbcols) :
                            if colonnes[pos_var_id][i]== var :
                                somme[j]+=colonnes[pos_col[j]][i]
                    rows.append([var] + somme)
        elif self.__func == "moyenne" :
            for var in colonnes[pos_var_id]:
                somme = [0 for _ in range(nbcols)]
                n = [0 for _ in range(nbcols)]
                if var not in liste_differents :
                    liste_differents.append(var)
                    for i in range(len(colonnes[pos_var_id])):
                        for j in range(nbcols) :
                            if colonnes[pos_var_id][i]== var :
                                somme[j]+=colonnes[pos_col[j]][i]
                                n[j]+=1
                    rows.append([var] + [somme[k]/n[k] for k in range(nbcols)])
        #print(liste_differents)
        table_new = Table([self.__var_id] + [self.__func + " " + col for col in cols],rows)
        return table_new
            

