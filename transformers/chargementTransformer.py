# -*- coding: utf-8 -*-
from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from datetime import date
import csv
import json
from copy import deepcopy

class ChargementTransformer(AbstractTransformers) :
    """
    transformaeur qui va charger les fichiers dans une table
    pour etre ensuite traitee.
    On pourra charger les fichiers de type csv ou json

    Args:
            chemin_fichier (str): le chemin vers le fichier qu'on veut charger
            types_var(list(str)) : liste des types de chacunes des variables (colonnes). Les types peuvent etre : "str", "int", "float" ou  "date"
                                    Default to None. Si None toutes les colonnes sont des str. 
            nom_feuille (str, optional) : la sous-partie du fichier json que l'on souhaite garder. Default to None
    """

    def __init__(self,chemin_fichier,type_fichier,types_var=None,nom_feuille=None):
        """
        construit un transformeur de type chargement. 
        Ce transformeur va charger les donnees a partir d'un chemin de fichier.
        Sont pris en charge les fichiers csv et json

        Args:
            chemin_fichier (str): le chemin vers le fichier qu'on veut charger

            types_var (list(str)) : liste des types de chacunes des variables (colonnes). Les types peuvent etre : "str", "int", "float" ou  "date"
                                    Default to None. Si None toutes les colonnes sont des str. 
            nom_feuille (str, optional) : la sous-partie du fichier json que l'on souhaite garder. Default to None
        """
        self.__chemin_fichier = chemin_fichier
        self.__type_var = types_var
        self.__type_fichier = type_fichier
        self.__nom_feuille=nom_feuille
    
    def chargement_csv(self):
        """
        methode qui va charger le fichier csv dans une table.

        Args :
            table (Table) : table vide que l'on va remplir avec les donnees chargees. Default to Table([],[]).

        Return :
            Table : tableau contenant les informations du fichier csv
        """
        table=Table([],[])
        data = []
        nb_line = 0
        with open (self.__chemin_fichier,encoding = 'UTF-8') as csvfile :
            covidreader = csv.reader(csvfile, delimiter = ";")
            for line in covidreader :
                row = []
                if nb_line == 0 :
                    table.column_names = line
                    nb_line+=1
                else :
                    for i in range(len(line)) :
                        if self.__type_var[i] == None :
                            row.append(line[i])
                        elif self.__type_var[i] == "str":
                            row.append(str(line[i]))
                        elif self.__type_var[i] == "int":
                            row.append(int(line[i]))
                        elif self.__type_var[i]=="float":
                            row.append(float(line[i]))
                        elif self.__type_var[i]== "date":
                            row.append(date.fromisoformat(line[i]))
                    table.add_row(row)
                        
                    
        return table

    #j'ai juste repris le code donne du prof mais la methode n'est pas finie
    def chargement_json(self):
        """
        methode qui va charger le fichier json dans une table.

        Return :
            Table : tableau contenant les informations du fichier json
        """
        table=Table([],[])
        with open(self.__chemin_fichier,encoding = 'UTF-8') as json_file :
            test2 = deepcopy(json.load(json_file))
            if self.__nom_feuille not in list(test2.keys()):
                print("Le nom de feuille demandé n'est pas dans votre fichier .JSON")
            else :
                test=test2.get(self.__nom_feuille)
                liste_var=list(test[0].keys())
                table.column_names=list(test[0].keys())
                
                for i in (range(0,len(test))):
                    varListe=list(test[i].keys())
                    for j in varListe:
                        if j not in liste_var:
                            print ("Variable de la ligne ",i, +" n'as pas sa place")

                    ligne=[]
                    donnee=list(test[i].values())
                    for k in liste_var:
                        val=test[i].get(k)
                        ligne.append(val)
                    
                    table.add_row(ligne)
        return(table)

    def transform(self,table = Table([],[])):
        """
        Charge les informations de fichiers de types soit csv soit json dans une table.

        Args:
            table ([Table optional): table vide. Defaults to Table([],[]).

        Returns:
            Table: table avec les informations contenus dans le fichier selectionne
        """
        if self.__type_fichier == "csv" or self.__type_fichier == "CSV" :
            return self.chargement_csv()

        elif self.__type_fichier == "json" or self.__type_fichier == "JSON" :
            return self.chargement_json()



