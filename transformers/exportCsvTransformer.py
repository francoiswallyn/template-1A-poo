from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils
import csv

class ExportCsvTransformer (AbstractTransformers) :
    """
    Transformateur qui exporte une table en fichier csv. 

    Args:
        chemin (str): le chemin vers le dossier ou l'on veut sauvegarder le fichier csv
        nom (str): le nom que l'on veut donner au fichier csv
    """

    def __init__ (self, chemin, nom) : 
        """
        Construit l'exportateur de table au format csv.

        Args:
            chemin (str): le chemin vers le dossier ou l'on veut sauvegarder le fichier csv
            nom (str): le nom que l'on veut donner au fichier csv
        """
        self.chemin = chemin 
        self.format = format
        self.nom = nom

    def transform(self, table) :
        """
        Exporte la table passee en parametre au format csv

        Args:
            table (Table): la table que l'on souhaite exporter
        
        Returns :
            Table : la table non modifiee
        """
        tot = self.chemin + "\\" + self.nom 
        with open (self.chemin + "/" + self.nom + ".csv", 'w', newline = '', encoding = 'UTF-8') as csvfile :
            covidwriter = csv.writer(csvfile, delimiter = ",")
            covidwriter.writerow(table.column_names)
            for i in table.rows : 
                covidwriter.writerow(i)
        return(table)
        

        

            

