from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils

class ReductionTransformer(AbstractTransformers) :
    """
    transformeur qui va reduire un tableau ayant des valeurs numeriques
    """

    def __init__(self):
        """
        construit un transformeur qui va reduire un tableau 
        """
        pass 

    def vect_sd(self,table):
        """
        renvoie la liste des ecart-types de chaque colonne

        Args:
            table (Table): la table dont on veut les ecart-types
        """
        colonnes = table.get_columns()
        sd = []
        for col in colonnes :
            sd.append(Outils.sd(col))
        return sd

    def transform(self,table) :
        """
        transformeur qui reduit la table en entree.
        Il faut que la table soit composee de valeurs numeriques.

        Args:
            table (Table): la table que l'on veut reduire

        Return:
            Table : la table reduite 
        """
        #On calcule les ecart-types pour chaque colonne
        sd = self.vect_sd(table)
        #On parcourt les lignes
        for i in range(len(table.rows)) :
            #on parcourt les colonnes
            for j in range(len(table.column_names)):
                #on reduit l'element de la ligne i pour la colonne j 
                table.rows[i][j] = table.rows[i][j]/sd[j]
        return table 