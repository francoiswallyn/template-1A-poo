from transformers.abstractTransformers import AbstractTransformers
from transformers.centrageTransformer import CentrageTransformer
from transformers.reductionTransformer import ReductionTransformer
from pipeline.table import Table

class NormalisationTransformer(AbstractTransformers) :
    """
    transformeur qui va normaliser une table : centrage + reduction. 
    """

    def __init__(self):
        pass

    def transform(self,table):
        """
        normalise la table en entree. Il faut que la table soit composee
        de valeurs numeriques

        Args:
            table (Table): table que l'on veut normaliser

        Return :
            Table : la table normalisee
        """
        ReductionTransformer().transform(CentrageTransformer().transform(table))
        return table
        #cette methode renvoie la table centree puis reduite

