from transformers.abstractTransformers import AbstractTransformers
from pipeline.table import Table
from pipeline.outils import Outils
from copy import deepcopy

class JointureTransformer(AbstractTransformers):
    """
    Transformeur qui va faire la jointure entre deux table. On peut faire une jointure interne ou une jointure gauche.
    Il faut que cess tables soit chargees au prealable.

    Args:
        table (Table): la table que l'on veut joindre a notre table de depart
        cle_primaire (str): le nom de la cle primaire dans la table de depart, pas besoin que ce soit une cle primaire si jointure gauche
        cle_primaire2 (str): le nom de la cle primaire dans la table que l'on veut joindre
        typejointure (str,optional): le type de la jointure "inner" pour une jointure interne
                                                            "left" pour une jointure a gauche
    """
    def __init__(self,table,cle_primaire,cle_primaire2,typejointure ="inner"):
        """
        Construit le transformateur de la jointure en fonction du type de jointure et des colonnes 
        sur lesquelles on veut faire la jointure.

        Args:
            table (Table): la table que l'on veut joindre a notre table de depart
            cle_primaire (str): le nom de la cle primaire dans la table de depart, pas besoin que ce soit une cle primaire si jointure gauche
            cle_primaire2 (str): le nom de la cle primaire dans la table que l'on veut joindre
            typejointure (str,optional): le type de la jointure "inner" pour une jointure interne
                                                                "left" pour une jointure a gauche
        """
        self.cle_primaire=cle_primaire
        self.cle_primaire2=cle_primaire2
        self.table1=table
        self.__typejointure=typejointure
        
        
    
    
    def inner_join(self,table2):
        """
        jointure interne entre table2 (table de depart) et la table passee en parameter de l'objet Jointure

        Args:
            table2 (Table): table sur laquelle on veut joindre la table passee en parametre du constructeur
        
        Returns :
            Table : la table avec la jointure interne
        """
        #Etape 1 test si la cle primaire est dans les 2 tables
        ## Test avec la premiere liste de liste de chaque table "self.cle_primaire" in (liste1 and liste2)
        
        if self.cle_primaire in (self.table1.column_names and table2.column_names):
            ##Si Oui : on continu étape 2
            
            #Etape 2 : Savoir dans quel position se trouve la colonne clé primaire dans chaque table

            position1=self.table1.column_names.index(self.cle_primaire)
            position2=table2.column_names.index(self.cle_primaire)

             #Etape 3 : On transpose les tables avec la fonction crée classe Table
            transpose1=self.table1.get_columns()
            transpose2=table2.get_columns()
            
            #Etape 4 : création de la grande table, liste de liste a un element dans chaque liste, le nom de chaque variable (pas de double)
            t=self.table1.column_names+table2.column_names
            try:
                while True:
                    t.remove(self.cle_primaire)
            except ValueError:
                pass
            t=[self.cle_primaire]+t
            
            
            var_new_table=[]
            for i in t:
                if i not in var_new_table:
                    var_new_table.append(i)
                else:
                    var_new_table.append(i+"1")
            

            TableJoin=Table(var_new_table,[])
            
            
            for i in range (len(transpose1[0])):#nombre d'individu table 1
                ligne=[None]*len(var_new_table)
                for j in self.table1.column_names: #nombre de variables

                    position=self.table1.column_names.index(j)
                    val=transpose1[position][i]
                    pos=TableJoin.column_names.index(j)
                    ligne[pos]=val
                    
                TableJoin.add_row(ligne)
            
            
            #copier la table 2

            #for 
            #if -> cléprimaire = remplie  
            #else -> nouvelle ligne A,D,E et None en B,C
            
            for i in table2.rows:
                if i[position2] in TableJoin.get_columns()[0]:
                    #a quel endroit est cet individu
                    posind=TableJoin.get_columns()[position2].index(i[position2])
                    modif=TableJoin.rows[posind]
                    for j in table2.column_names: 
                        
                        position=table2.column_names.index(j)
                        val=i[position]
                        
                        if j+"1" in TableJoin.column_names:
                            pos=TableJoin.column_names.index(j+"1")
                        else:
                            pos=TableJoin.column_names.index(j)
                        modif[pos]=val

                    TableJoin.rows[posind]=modif
            data=TableJoin.rows
            for d in data:
                if None in d:
                    del data[data.index(d)]
            TableJoin.rows=data
            #Etape 5, ajoue des individus de la table 1 dans la grande table
            
        else : 
            ## Si non : stop probleme
            print("probleme")

        return(TableJoin)
    def left_join(self,tableDepart):
        """
        jointure gauche entre la table de depart et la table passee en parametre du constructeur.

        Args:
            tableDepart (Table): table a laquelle on veut joindre la table passee en parametre du constructeur
        
        Returns :
            Table : la table qui est la jointure gauche entre tableDepart et la table passee en parametre du constructeur
        """
        #etape 1, position des "cles_primaires"
        position1=tableDepart.column_names.index(self.cle_primaire)
        position2=self.table1.column_names.index(self.cle_primaire2)
        #liste des variables cle primaire table 1
        transpose=tableDepart.get_columns()[position1]
        #liste des variable de l'autre table
        transpose2=self.table1.get_columns()[position2]

        var_table2=self.table1.column_names
        var_table2.remove(self.cle_primaire2)

        new_table=Table(tableDepart.column_names+var_table2,[])
        compteur=0
        for i in transpose:
            if len(i)==1:
                i="0"+i
            numeroligne=transpose2.index(i)
            ligne=deepcopy(self.table1.rows[numeroligne])
            del ligne[position2]
            new_table.add_row(tableDepart.rows[compteur]+ligne)
            compteur+=1

        return(new_table)




    def transform(self,tableDepart):
        """
        Fait la jointure entre la table de depart et la table passee en parametre du constructeur
        en fonction du type de jointure choisie.

        Args:
            tableDepart (Table): table a laquelle on veut joindre la table passee en parametre du constructeur

        Returns:
            Table: table qui est la jointure en la table de depart et la table passee en parametre du constructeur
        """
        if self.__typejointure == "inner" or self.__typejointure == "INNER" :
            return self.inner_join(tableDepart)

        elif self.__typejointure == "left" or self.__typejointure == "LEFT" :
            return self.left_join(tableDepart)