from pipeline.abstractOperation import AbstractOperation
from pipeline.table import Table

class Pipeline :
    """
    Le pipeline permet d'effectuer une suite d'oprations sur des donnees. Les operations peuvent etre des transformations 
    ou des estimations. Il faut d'abord charger les donnees. 

    Args:
            steps (list(AbstractOperation): liste d'opérations appliquees sur les donnees dans l'ordre.
                                            la premiere etape doit être un chargement si on a pas de table de depart
                                            
    """



    def __init__(self,steps):
        """
        Construit un pipeline de donnees 

        Args:
            steps (list(AbstractOperation): liste d'opérations appliquees sur les donnees dans l'ordre.
                                            la premiere étape doit etre un chargement si on a pas de table de depart

        """
        self.__steps = steps


    
    def add_step(self,step):
        """
        ajoute une etape au pipeline de donnees

        Args:
            step (AbstractOperation): nouvelle operation (transformation ou estimation) 
        """
        self.__steps.append(step)

    def run(self,table = Table([],[])):
        """
        execute chacune des operations dans l'ordre sur les donnees, a commencer par le chargement

        Returns :
            Table : la table après transformations et estimations de la table de depart
        """
        for step in self.__steps :
            table  = step.process(table)

        return table



