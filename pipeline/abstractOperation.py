from abc import abstractmethod
from pipeline.table import Table


class AbstractOperation:
    """
    classe abstraite qui va servir d'interface 
    """
    def __init__(self):
        pass

    @abstractmethod
    def process(self, data):
        """
        methode abstraite que doit contenir les classes filles Estimators et 
        Transformers pour etre utilisee dans le pipeline

        Args:
            data (Table): la table que l'on veut traiter
        Return :
            Table : la table traitee
        """
        pass
