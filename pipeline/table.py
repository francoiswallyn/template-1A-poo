class Table :
    """
    Construit une table
    """
    def __init__(self,column_names,rows):
        """
        construit un tableau 

        Args:
            column_names (list(str)): liste des noms de colonnes
            rows (list(list)): listes des lignes (une ligne  = une liste)
        """
        self.column_names = column_names
        self.rows = rows


    def add_row(self,row,position=-1):
        """
        ajoute une ligne a la position voulue (position premiere ligne  = 0), 
        par defaut apres la derniere ligne
        
    

        Args:
            row (list): la ligne a ajouter doit etre de longueur nb de colonnes
            position (int): position de la ligne,par defaut -1
        """
        if position == -1 or position ==len(self.rows):
            self.rows.append(row)
        elif 0 <= position <= len(self.rows)-1 :
            self.rows.insert(position,row)
        else :
            raise Exception("position impossible")

    def remove_row(self,position =-1):
        """
        retire la ligne situee a la position choisie (position premiere ligne  = 0)

        Args:
            position (int, optional): position de la ligne que l'on veut enlever. Defaults to -1.
        """
        if position == -1 :
            self.rows.pop()
        elif 0<=position<=len(self.rows)-1 :
            self.rows.pop(position)
        else :
            raise Exception("position impossible")
    
    def add_column(self,name,column,position = -1):
        """
        ajoute une colonne a la position voulue

        Args:
            name (str): [nom de la colonne]
            column (list): colonne a ajouter
            position (int, optional): position de la colonne. Defaults to -1.
        """
        if position == -1 or position ==len(self.column_names):
            self.column_names.append(name)
            for i in range(len(self.rows)) :
                self.rows[i].append(column[i])
        if 0<=position<=len(self.column_names)-1 : 
            self.column_names.insert(position, name)
            for i in range(len(self.rows)) :
                #on ajoute chaque element de la colonne dans chaque ligne a la position souhaitee
                self.rows[i].insert(position,column[i]) 

    
    def remove_column(self,position=-1):
        """
        retire la colonne siuee a la position chosie.
        La position de la premiere colonne est 0. 

        Args:
            position (int, optional): position de la colonne qu'on veut retirer. Defaults to -1.
        """
        if position == -1 :
            self.column_names.pop()
            for i in range(len(self.rows)) :
                self.rows[i].pop()
        if 0<=position<=len(self.column_names)-1: 
            self.column_names.pop(position)
            for i in range(len(self.rows)) :
                #on retire l'element situe a la position choisie de chaque ligne
                self.rows[i].pop(position) 

    def remove_column_name(self,name) :
        """
        retire la colonne qui a le nom choisi.
        Si 2 colonnes ont le meme nom, la premiere sera retiree.

        Args:
            name (str): nom de la colonne que l'on veut retirer
        """
        position = self.column_names.index(name)
        self.remove_column(position=position)

    def get_columns(self):
        """
        Renvoie la liste des colonnes. La table est construite a partir des lignes
        ici on va recuperer une liste de listes qui ont pour element la valeur de la colonne.
        Utile pour le centrage du tableau notamment.

        Return : 
            list(list) : la liste des colonnes
        """
        columns= []
        for i in range(len(self.column_names)):
            col =[]
            for row in self.rows :
                col.append(row[i])
            columns.append(col)
        return columns


    def levels(self,col):
        """
        Renvoie les differentes valeurs que prend une colonne.

        Args:
            col ("str"): nom de colonne dont on veut les valeurs

        Returns:
            list: la liste des valeurs differente que prend la colonne
        """
        levels = []
        pos = self.column_names.index(col)
        for row in self.rows :
            if row[pos] not in levels :
                levels.append(row[pos])
        return levels

    def get_types(self):
        types = []
        for i in range(len(self.column_names)):
            types.append(type(self.rows[0][i]))
        return types
