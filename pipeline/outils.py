from math import sqrt

class Outils :
    """
    On definit ici les outils que l'on va utiliser pour construire. Ce sont des methodes statiques
    a la fois les transformeurs et estimateurs tels que : 
    la moyenne d'une liste de reels
    l' ecart type d'une liste reels
    les méthode de calcul de distance entre des vecteurs
    """

    def __init__(self):
        pass
    
    @staticmethod
    def sum(liste):
        """
        Renvoie la somme d'une liste de reels

        Args:
            liste (list(float or int)): une liste de reels

        Returns:
            float: la somme des elements de la liste
        """
        sum = 0
        for number in liste :
            sum+=number
        return float(sum)
    @staticmethod
    def mean(liste):
        """
        Renvoie la moyenne d'une liste de reels

        Args:
            liste (list(float or int)): une liste de reels
        
        Return:
            float : la moyenne de la liste
        """
        sum = Outils.sum(liste)
        n =len(liste)
        mean = sum/n
        return mean

    @staticmethod
    def sd(liste):
        """
        Renvoie l'ecart type d'une liste de reels

        Args:
            liste (list(float)): une liste de reels
        Return:
            float : la moyenne de la liste
        """
        var = 0 
        m = Outils.mean(liste)
        for number in liste :
            var += (number-m)**2
        var = var/len(liste)
        sd = sqrt(var)
        return sd
    
    @staticmethod
    def norme(liste) : 
        """
        Renvoie la norme euclidienne d'une liste de reels

        Args:
            liste (list(float)): liste de reels
        
        Return :
        float
        """
        norme = 0
        n = len(liste)
        for i in range(n) : 
            norme += liste[i]**2
        return(norme**0.5)
    
    @staticmethod
    def  vect_norme(liste) :
        """
        Renvoie le vecteur des normes pour chaque sous-liste.

        Args:
            liste (list(list)): liste de liste vu comme une liste de vecteurs
        
        Return :
        list(float)
        """
        vect = []
        for i in range(len(liste)) :
            vect.append(Outils.norme(liste[i]))
        return(vect)

    @staticmethod
    def distance(liste1, liste2) : 
        """
        Renvoie la distance euclidienne entre 2 vecteurs. Chaque liste est vue comme un vecteur. 

        Args:
            liste1 (list(float)): une liste de reels
            liste2 (list(float)): une liste de reels
        
        Return :
        float
        """
        distance = []
        for i in range(len(liste1)) : 
            distance.append(liste1[i] - liste2[i])
        return(Outils.norme(distance))

    @staticmethod
    def distance_vect (liste1, liste2) :
        """
        Renvoie le vecteur des distances entre les vecteurs des listes.

        Args:
            liste1 (list(list)): liste de vecteurs
            liste2 (list(list)): liste de vecteurs
        
        Return :
        list(float)
        """
        vect = []
        for j in range(len(liste1)) : 
            vect.append(Outils.distance(liste1[j],liste2[j]))
        return(vect)
    
    @staticmethod
    def egal_vect(vect1, vect2) :
        """
        Renvoie si un vecteur est egal a un autre.

        Args:
            vect1 (list(float)): un vecteur
            vect2 (list(float)): un vecteur

        Returns:
            Bool: True si les vecteurs sont egaux. False sinon
        """
        for i in range(len(vect1)) : 
            if vect1[i] != vect2[i] : 
                return (False)
        return(True)
    
