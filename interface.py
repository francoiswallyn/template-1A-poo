from __future__ import print_function, unicode_literals
import datetime
from datetime import date, timedelta
from copy import deepcopy
from transformers.chargementTransformer import ChargementTransformer
from transformers.centrageTransformer import CentrageTransformer
from transformers.reductionTransformer import ReductionTransformer
from transformers.normalisationTransformer import NormalisationTransformer
from transformers.selectionTransformer import SelectionTransformer
from transformers.jointureTransformer import JointureTransformer
from transformers.moyenneGlissanteTansformer import MoyenneGlissanteTransformer
from transformers.applicationSousTableTransformer import ApplicationSousTableTransformer
from transformers.exportCsvTransformer import ExportCsvTransformer
from transformers.exportMapTransformer import ExportMapTransformer
from estimators.meanEstimator import MeanEstimator
from estimators.sumEstimator import SumEstimator
from estimators.sdEstimator import SdEstimator
from pipeline.pipeline import Pipeline
from pipeline.table import Table
from pipeline.outils import Outils
from estimators.kmeansEstimator import kmeansEstimator
from transformers.agregationspatialTransformer import AgregationspatialTransformer



from pprint import pprint

from PyInquirer import prompt, Separator



def main():
    print("Vous allez pouvoir traiter vos donnees") 
    ask_etape()

def ask_type_etape():
    type_etape_prompt = {
    'type': 'list',
    'name': 'type etape',
    'message': 'Souhaitez-vous faire une transformation ou une estimation ?',
    'choices': ['Transformation', 'Estimation',Separator(),"Fin"]
    }
    answer = prompt(type_etape_prompt)
    return answer["type etape"]


def ask_etape(table = Table([],[])):
    answer_type_etape = ask_type_etape()
    if answer_type_etape == "Transformation":
        etape_prompt = {
        'type': 'list',
        'name': 'etape',
        'message': 'Quelle transformation ?',
        'choices': ['Chargement csv', 'Chargement json','Selection','Jointure',"Application sur sous table","Moyenne glissante","Normalisation","Export csv","Export map",Separator(),"Retour"]
        }
        answer = prompt(etape_prompt)
        if answer['etape'] == 'Chargement csv':
            op = ask_chargement_csv()
        elif answer['etape'] == 'Chargement json':
            op = ask_chargement_json()
        elif answer['etape'] == 'Selection':
            op = ask_selection(table)
        elif answer['etape'] == 'Application sur sous table':
            op = ask_application_sous_table(table)
        elif answer['etape'] == 'Jointure':
            op = ask_jointure(table)
        elif answer['etape'] == 'Moyenne glissante':
            op = ask_moy_glissante()
        elif answer['etape'] == 'Normalisation':
            op = normalisation()
        elif answer['etape'] == 'Export csv':
            op = ask_export_csv()
        elif answer['etape'] == 'Export map':
            op =ask_export_map(table)
        elif answer['etape'] == "Retour":
           return ask_etape(table)


    elif answer_type_etape == "Estimation":
        etape_prompt = {
        'type': 'list',
        'name': 'etape',
        'message': 'Quelle estimation ?',
        'choices': ['Moyenne', 'Somme',"Ecart type","Kmeans",Separator(),"Retour"]
        }
        answer = prompt(etape_prompt)
        if answer['etape'] == "Moyenne":
            op = ask_moyenne()
        elif answer['etape'] == "Ecart Type":
            op = ask_sd()
        elif answer['etape'] == "Ecart Type":
           op = ask_sum()
        elif answer['etape'] == "Kmeans":
           op = ask_kmeans()
        elif answer['etape'] == "Retour":
           return ask_etape(table)
    else :
        return table

    table = op.process(table)
    print(table.rows[0])
    ask_etape(table)

##########################################################
def ask_chargement_csv():
    question = [{
            'type': 'input',
            'name': 'chemin',
            'message': 'Nom du chemin vers le fichier ?',
            },
            {
            'type': 'input',
            'name': 'type_var',
            'message': 'Types des variables (separe par une ",") ?'
            }]
    answer = prompt(question)
    return ChargementTransformer(answer["chemin"],"csv",str_to_list(answer["type_var"]))

def ask_selection(table):
    question_col_line = [{
            'type': 'input',
            'name': 'colonne',
            'message': 'Quelles colonnes souhaitez vous garder (nom separes par "," , None si toutes) ?'
            },
            {
            'type': 'input',
            'name': 'lignes',
            'message': 'Quelles lignes (positions des lignes separes par "," ou None si toutes) ?'
            }]
    answer_col_line = prompt(question_col_line)
    answer_var_id_inter = ask_var_id_inter(table)
    answer_var_id_exact = ask_var_id_exact(table)

    return SelectionTransformer(str_to_list(answer_col_line["colonne"]),str_to_list(answer_col_line["lignes"]),str_to_dict_inter(answer_var_id_inter["var_id_inter"]+":"+answer_var_id_inter["inter_valeur"],table),str_to_dict_exact(answer_var_id_exact["var_id_exact"]+":"+answer_var_id_exact["valeurs"],table))

def ask_var_id_inter(table):
    question = [{
            'type': 'list',
            'name': 'var_id_inter',
            'message': 'Variable identifiante pour un intervalle de valeurs ?',
            'choices' : table.column_names + ['None']
            },
            {
            'type': 'input',
            'name': 'inter_valeur',
            'message': 'Intervalle de valeurs pour la colonne selectionnée'
            }]
    answer = prompt(question)
    return answer
def ask_var_id_exact(table):
    question = [{
            'type': 'list',
            'name': 'var_id_exact',
            'message': 'Variable identifiante qui prend une ou plusieurs valeurs ?',
            'choices' : table.column_names + ['None']
            },
            {
            'type': 'input',
            'name': 'valeurs',
            'message': 'Valeurs prises pour la colonne selectionnée (separées par ",")'
            }]
    answer = prompt(question)
    return answer

def str_to_list(string):
    if string != "None" :
        liste = []
        mot = ""
        for char in string :
            if char == ",":
                liste.append(mot)
                mot =""
            else :
                mot += char
        liste.append(mot)
        return liste
    else :
        return None
def str_to_dict_inter(string,table) :
    if string != "None" :
        key = ""
        i = 0
        while string[i] != ":":
            key += string[i]
            i+=1
        valeur = str_to_list(string[i+1:])
        valeur = str_to_list(string[i+1:])
        type_col = table.get_types()[table.column_names.index(key)]
        if type_col == date :
            if valeur[0] not in ["sup","inf"]:
                valeur[0] = date.fromisoformat((valeur[0]))
            valeur[1] = date.fromisoformat(valeur[1])
        else : 
            if valeur[0] not in ["sup","inf"]:
                valeur[0] = date.fromisoformat((valeur[0]))
            valeur[1] = date.fromisoformat(valeur[1])
        return {key : valeur}
    return string

def str_to_dict_exact(string,table) :
    if string != "None" :
        key = ""
        i = 0
        while string[i] != ":":
            key += string[i]
            i+=1
        valeur = str_to_list(string[i+1:])
        type_col = table.get_types()[table.column_names.index(key)]
        for val in valeur :
            val = type_col(val)
        return {key : valeur}
    return string

def ask_export_csv():
    question = [{
            'type': 'input',
            'name': 'nom',
            'message': 'Quelle nom de fichier ?',
            },
            {
            'type': 'input',
            'name': 'chemin',
            'message': 'Quel chemin de fichier ?'
            }]
    answer = prompt(question)
    return ExportCsvTransformer(answer['chemin'],answer['nom'])

def ask_chargement_json():
    question = [{
            'type': 'input',
            'name': 'chemin',
            'message': 'Nom du chemin vers le fichier ?',
            },
            {
            'type': 'input',
            'name': 'nom_feuille',
            'message': 'Quel sous fichier json souhaitez vous garder ?'
            }]
    answer = prompt(question)
    return ChargementTransformer(answer['chemin'],'json',None,answer['nom_feuille'])

def ask_moyenne():
    question = [{
            'type': 'input',
            'name': 'nom_colonne',
            'message': 'Noms des colonnes sur lesquelles on veut la moyenne (separes par "," ou None si toutes)',
            }]
    answer = prompt(question)
    return MeanEstimator(str_to_list(answer['nom_colonne']))

def ask_sd():
    question = [{
            'type': 'input',
            'name': 'nom_colonne',
            'message': "Noms des colonnes sur lesquelles on veut l'ecart type (separes par ',' ou None si toutes)",
            }]
    answer = prompt(question)
    return SdEstimator(str_to_list(answer['nom_colonne']))

def ask_sum():
    question = [{
            'type': 'input',
            'name': 'nom_colonne',
            'message': "Noms des colonnes sur lesquelles on veut la somme (separes par ',' ou None si toutes)",
            }]
    answer = prompt(question)
    return SumEstimator(str_to_list(answer['nom_colonne']))

def ask_export_map(table):
    question = [{
            'type': 'input',
            'name': 'nom',
            'message': 'Quel nom de fichier ?',
            },
            {
            'type': 'list',
            'name': 'echelle',
            'message': 'Quelle echelle ? ',
            'choices' : ["dep","reg"]
            },
            {
            'type': 'list',
            'name': 'nom_col_echelle',
            'message': "Quel nom de colonne pour l'echelle ? ",
            'choices' : table.column_names
            },
            {
            'type': 'list',
            'name': 'nom_col_plot',
            'message': "Quelle variable a representer ? ",
            'choices' : table.column_names
            },
            {
            'type': 'confirm',
            'name': 'is_metropole',
            'message': "Representer uniquement la metropole ?"
            }
            ]
    answer = prompt(question)
    return ExportMapTransformer(answer['nom'],answer['echelle'],answer['nom_col_echelle'],answer['nom_col_plot'],answer['is_metropole'])

def ask_application_sous_table(table):
    question = [{
            'type': 'list',
            'name': 'var_id',
            'message': 'Quelle variable identifiante ?',
            'choices' : table.column_names
            },
            {
            'type': 'list',
            'name': 'func',
            'message': 'Quelle fonction a appliquer ? ',
            'choices' : ["moyenne","somme"]
            },
            {
            'type': 'input',
            'name': 'noms_cols',
            'message': "Sur quelles colonnes appliquer la fonction ? (separer par ',' ou None si toutes)",
            }
            ]
    answer = prompt(question)
    return ApplicationSousTableTransformer(answer['var_id'],answer['func'],str_to_list(answer["noms_cols"]))

def ask_jointure(table):
    question_type = [{
        'type': 'list',
        'name': 'type_fichier',
        'message': 'Quel type de fichier pour la table a joindre?',
        'choices' : ["csv","json"]
    }
    ]
    answer_type = prompt(question_type)
    if answer_type["type_fichier"] == "csv":
        op = ask_chargement_csv()
        table_to_join = op.process(table)
    else :
        op = ask_chargement_json()
        table_to_join = op.process(table)
    
    question = [{
        'type': 'list',
        'name': 'type_join',
        'message': 'Quel type de jointure ?',
        'choices' : ["inner","left"]
        },
        {
        'type': 'list',
        'name': 'cle_1',
        'message': 'Quelle cle pour table de depart ?',
        'choices' : table.column_names
        },
        {
        'type': 'list',
        'name': 'cle_2',
        'message': 'Quelle cle pour la table à joindre ? ',
        'choices' : table_to_join.column_names
        }
        ]
    answer = prompt(question)
    return JointureTransformer(table_to_join,answer['cle_1'],answer['cle_2'],answer['type_join'])

def ask_kmeans():
    question = [{
        'type': 'input',
        'name': 'nclasse',
        'message': 'Combien de classes ?'
        },
        {
        'type': 'input',
        'name': 'nstart',
        'message': "Combien d'iterations pour l'algorithme ? (minimum 30)"
        }
        ]
    answer = prompt(question)
    return kmeansEstimator(int(answer['nclasse']),int(answer['nstart']))

def ask_moy_glissante():
    question = [{
        'type': 'input',
        'name': 'period',
        'message': 'Quelle période ?'
        }
        ]
    answer = prompt(question)
    return MoyenneGlissanteTransformer(int(answer['period']))

def normalisation():
    return NormalisationTransformer()
if __name__ == '__main__':
    main()